///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 07d - AnimaL Farm1 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm1
//////
////// file: deleteCats.c
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   03/08/2022
///////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>

#include "deleteCats.h"
#include "catDatabase.h"

bool deleteCat( const size_t index ){
	if( !isIndexValid( index ) ){
		return false;
	}
	else{
		swapCat( index, numCats - 1 ); //Swap last cat with delete cat
		wipeCat( numCats - 1 );

		numCats -= 1;
		return true;
	}
}

bool deleteAllCats(){
	while( numCats != 0 ){
		deleteCat( 0 );
	}

	numCats = 0;
	return true;
}


