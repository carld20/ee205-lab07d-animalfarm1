///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 07d - AnimaL Farm1 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm1
//////
////// file: catDatabase.h
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   03/08/2022
///////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h>
#include <stddef.h>

#define MAX_CATS (30)
#define MAX_CAT_NAME (30)


extern size_t numCats;

enum Genders { Male, Female, Unknown };
enum Breeds { UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX };
enum Color { Black, White, Red, Blue, Green, Pink };

struct Cat {
	char			name[MAX_CAT_NAME];
	enum Genders		gender;
	enum Breeds		breed;
	bool			isFixed;
	float			weight;
	enum Color		collarColor1;
	enum Color		collarColor2;
	unsigned long long	license;
};

extern struct Cat cats[MAX_CATS];

//Implement initializeDatabase
extern void initializeDatabase();

//Implement isFull
extern bool isFull();

//Implement validateDatabase
extern bool validateDatabase();

//Implement isIndexValid
extern bool isIndexValid( const size_t index );

//Implement isNameValid
extern bool isNameValid( const char* name );

//Implement isWeightValid
extern bool isWeightValid( const float weight );

//Implement wipeCat
extern void wipeCat( const size_t index );

//Implement swapCat
extern bool swapCat( const size_t x, const size_t y);

