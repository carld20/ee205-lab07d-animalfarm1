///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 07d - AnimaL Farm1 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm1
//////
////// file: updateCats.h
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   03/08/2022
///////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <stdbool.h>

#include "catDatabase.h"

extern bool updateCatName( const size_t index, const char* name );
extern bool fixCat( const size_t index );
extern bool updateCatWeight( const size_t index,  const float weight );
extern bool updateCatCollar1( const size_t index, const enum Color color );
extern bool updateCatCollar2( const size_t index, const enum Color color );
extern bool updateLicense( const size_t index, const unsigned long long license );

 

