///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 07d - AnimaL Farm1 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm1
//////
////// file: reportCats.h
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   03/08/2022
///////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "catDatabase.h"

extern void printCat( const size_t index );
extern void printAllCats();
extern size_t findCat( const char* name );
extern char* genderCat( const enum Genders gender );
extern char* breedCat( const enum Breeds breed );
extern char* colorCat( const enum Color color );

