###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 07d - animalFarm0 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
###
### @author Carl Domingo <carld20@hawaii.edu>
### @date   03/08/2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra

TARGET = main

all: $(TARGET)
addCats.o: addCats.c addCats.h
	$(CC) $(CFLAGS) -c addCats.c

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

main.o: main.c
	$(CC) $(CFLAGS) -c main.c

main: main.o addCats.o catDatabase.o deleteCats.o reportCats.o updateCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o catDatabase.o deleteCats.o reportCats.o updateCats.o

clean:
	rm -f $(TARGET) *.o

test: $(TARGET)
	./$(TARGET)
