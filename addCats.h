///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 07d - AnimaL Farm1 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm1
//////
////// File:addCats.h
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   03/08/2022
///////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "catDatabase.h"

extern unsigned int addCat (const char* newName,
		const enum Genders newGender,
		const enum Breeds newBreed,
		const bool newFixed,
		const float newWeight,
		const enum Color newCollarColor1,
		const enum Color newColllarColor2,
		const unsigned long long newLicense
		);


